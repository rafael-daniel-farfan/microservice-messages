package com.pragma.powerup.messagesmicroservice.adapters.driving.http.handler.impl;

import com.pragma.powerup.messagesmicroservice.adapters.driving.http.dto.response.MessageResponseDto;
import com.pragma.powerup.messagesmicroservice.adapters.driving.http.handler.IMessageHandler;
import com.pragma.powerup.messagesmicroservice.adapters.driving.http.mapper.IMessageResponseMapper;
import com.pragma.powerup.messagesmicroservice.domain.api.IMessageServicePort;
import com.pragma.powerup.messagesmicroservice.domain.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MessageHandlerImpl implements IMessageHandler {

    private final IMessageServicePort messageServicePort;

    private final IMessageResponseMapper messageResponseMapper;

    @Override
    public MessageResponseDto sendSMS() {

        Message sms = messageServicePort.sendSMS();
        return messageResponseMapper.toResponse(sms);
    }
}
