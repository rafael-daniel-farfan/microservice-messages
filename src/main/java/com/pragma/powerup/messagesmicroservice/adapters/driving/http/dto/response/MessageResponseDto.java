package com.pragma.powerup.messagesmicroservice.adapters.driving.http.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class MessageResponseDto {
    @JsonProperty("message")
    private String message;
    @JsonProperty("pin")
    private String pin;
}
