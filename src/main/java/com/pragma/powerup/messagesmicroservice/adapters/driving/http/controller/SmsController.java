package com.pragma.powerup.messagesmicroservice.adapters.driving.http.controller;


import com.pragma.powerup.messagesmicroservice.adapters.driving.http.dto.response.MessageResponseDto;
import com.pragma.powerup.messagesmicroservice.adapters.driving.http.handler.IMessageHandler;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
@RequiredArgsConstructor
public class SmsController {
    private final IMessageHandler handler;
    @GetMapping(value = "/sendSMS")
    public ResponseEntity<MessageResponseDto> sendSMS() {
       MessageResponseDto sms = handler.sendSMS();
        return ResponseEntity.status(HttpStatus.CREATED).body(sms);
    }
}
