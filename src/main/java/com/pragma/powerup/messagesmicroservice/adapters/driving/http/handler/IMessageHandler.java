package com.pragma.powerup.messagesmicroservice.adapters.driving.http.handler;

import com.pragma.powerup.messagesmicroservice.adapters.driving.http.dto.response.MessageResponseDto;

public interface IMessageHandler {

    MessageResponseDto sendSMS();
}
