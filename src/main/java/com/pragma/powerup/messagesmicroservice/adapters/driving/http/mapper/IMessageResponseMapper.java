package com.pragma.powerup.messagesmicroservice.adapters.driving.http.mapper;

import com.pragma.powerup.messagesmicroservice.adapters.driving.http.dto.response.MessageResponseDto;
import com.pragma.powerup.messagesmicroservice.domain.model.Message;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IMessageResponseMapper {

    MessageResponseDto toResponse(Message message);


}
