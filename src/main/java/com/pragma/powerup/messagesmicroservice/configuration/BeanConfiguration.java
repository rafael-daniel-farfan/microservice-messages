package com.pragma.powerup.messagesmicroservice.configuration;

import com.pragma.powerup.messagesmicroservice.domain.api.IMessageServicePort;
import com.pragma.powerup.messagesmicroservice.domain.usecase.MessageUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {


    @Bean
    public IMessageServicePort messageServicePort() {

        return new MessageUseCase();
    }

}
