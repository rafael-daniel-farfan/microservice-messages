package com.pragma.powerup.messagesmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class MessagesMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessagesMicroserviceApplication.class, args);
	}

}
