package com.pragma.powerup.messagesmicroservice.domain.api;

import com.pragma.powerup.messagesmicroservice.domain.model.Message;

public interface IMessageServicePort {

    Message sendSMS();

}
