package com.pragma.powerup.messagesmicroservice.domain.usecase;

import com.pragma.powerup.messagesmicroservice.domain.api.IMessageServicePort;
import com.pragma.powerup.messagesmicroservice.domain.model.Message;
import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;


import java.util.Random;


public class MessageUseCase implements IMessageServicePort {

    public static final String ACCOUNT_SID = "ACefd68b1542382aa89263ad066c9fd9cc";
    public static final String AUTH_TOKEN = "34686203b49bd24c687ef6f02226de0d";

    private Random random = new Random();

    @Override
    public Message sendSMS() {
        String pin = String.valueOf(1000 + random.nextInt(9000));
        String messageText = "Tú Pedido está listo!📞PIN: " + pin;

        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        com.twilio.rest.api.v2010.account.Message twilioMessage = com.twilio.rest.api.v2010.account.Message.creator(
                new PhoneNumber("+573173982019"),
                new PhoneNumber("+14302456920"),
                messageText
        ).create();

        Message sms = new Message(messageText, pin);

        return sms;
    }
}
