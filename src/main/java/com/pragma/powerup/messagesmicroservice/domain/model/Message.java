package com.pragma.powerup.messagesmicroservice.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {

    @JsonProperty
    private String message;
    @JsonProperty
    private String pin;

    public Message() {

    }

    public Message(String message, String pin) {
        this.message = message;
        this.pin = pin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
