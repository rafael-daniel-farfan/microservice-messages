<br />
<div align="center">
<h3 align="center">MESSENGER SERVICE MICROSERVICE</h3>
  <p align="center">
    In this repository, you will find messenger service microservice implementation.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)
* ![Twilio](https://seeklogo.com/images/T/twilio-logo-3C6732AED8-seeklogo.com.png)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)


### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repository
2. Change directory
   ```sh
   cd to project folder 
   ```
3. Run with intelli J idea or your IDE of preference.

<!-- USAGE -->

## Usage Postman to test messages

1. Open Postman
2. import SMS-CONTROLLER.postman_collection.json
3. Open collection and make a request in http://localhost:9000/sendSMS
4. You will receive as a response "Message sent successfully" and the http status code 200 ok.


